const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const Photo = require('../models/Photo');
const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
  router.get('/', (req, res) => {
    Photo.find().populate('author')
      .then(photos => res.send(photos))
      .catch(error => res.status(400).send(error))
  });

  router.get('/:id', (req, res) => {
    Photo.find({author: req.params.id}).populate('author')
      .then(photos => res.send(photos))
      .catch(error => res.status(400).send(error))
  });

  router.post('/', upload.single('image'), (req, res) => {
    const photoData = req.body;

    if (req.file) {
      photoData.image = req.file.filename;
    } else {
      photoData.image = null;
    }

    const photo = new Photo(photoData);

    photo.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.delete('/', (req, res) => {
    console.log(req.body);
    Photo.findByIdAndRemove(req.body.id)
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error))
  });

  return router;
};

module.exports = createRouter;