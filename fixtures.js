const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Photo = require('./models/Photo');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropDatabase();
  } catch (e) {
    console.log('Database were not present, skipping drop...');
  }

  const [user, admin, john] = await User.create({
    username: 'user',
    password: 'user',
    role: 'user',
    displayName: 'user'
  }, {
    username: 'admin',
    password: 'admin',
    role: 'admin',
    displayName: 'admin'
  }, {
    username: 'john_doe',
    password: 'john',
    role: 'user',
    displayName: 'John Doe'
  });

  await Photo.create({
    title: 'Century Child',
    image: 'Century_Child.jpg',
    author: user._id
  }, {
    title: 'Nightwish',
    image: 'Nightwish.jpg',
    author: user._id
  }, {
    title: 'Prodigy',
    image: 'Prodigy.jpg',
    author: john._id
  }, {
    title: 'The Fat Of The Land',
    image: 'The_Fat_Of_The_Land.jpg',
    author: john._id
  });

  db.close();
});