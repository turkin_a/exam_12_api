const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, '/public/uploads'),
  db: {
    url: 'mongodb://localhost:27017',
    name: 'photos'
  },
  facebook: {
    appId: "348921042300041",
    appSecret: "a8e88b73311f8e44e343e11368d13d73"
  }
};